//
//  ViewController.swift
//  gesture
//
//  Created by Oscar on 15/7/17.
//  Copyright © 2017 Oscar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var swipeVista: UIView!
    @IBOutlet var labelTap: UILabel!
    @IBOutlet var txtFieldTexto: UIView!
    @IBOutlet var campoTexto: UITextField!
    
    var numeroDeTap = 0
    var swipeReconocedorIzquierda = UISwipeGestureRecognizer()
    var swipeReconocedorDerecha = UISwipeGestureRecognizer()
    
    var tapReconocedor = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        swipeVista.layer.masksToBounds = true
        
        swipeReconocedorIzquierda = UISwipeGestureRecognizer.init(target: self, action: #selector(manejadorEvento))
        swipeReconocedorIzquierda.direction = UISwipeGestureRecognizerDirection.left
        
        swipeVista.addGestureRecognizer(swipeReconocedorIzquierda)
        
        //Derecha
        swipeReconocedorDerecha = UISwipeGestureRecognizer.init(target: self, action: #selector(manejadorEvento))
        swipeReconocedorDerecha.direction = UISwipeGestureRecognizerDirection.right
        
        swipeVista.addGestureRecognizer(swipeReconocedorDerecha)
        
        //Tap
        tapReconocedor = UITapGestureRecognizer.init(target: self, action: #selector(manejadorTap))
        
        swipeVista.addGestureRecognizer(tapReconocedor)
        
        //Agregar reconocedor de cambio de texto
        
        
    }

    @IBAction func reconocedorCambioTexto(_ sender: UITextField) {
        print(campoTexto.text! )
        if(campoTexto.text! == "izquierda"){
            let reconocedor = UISwipeGestureRecognizer()
            reconocedor.direction = UISwipeGestureRecognizerDirection.left
            manejadorEvento(sender: reconocedor)
        }
        
        if(campoTexto.text! == "derecha"){
            let reconocedor = UISwipeGestureRecognizer()
            reconocedor.direction = UISwipeGestureRecognizerDirection.right
            manejadorEvento(sender: reconocedor)
        }
        
        if(campoTexto.text! == "tap"){
            let reconocedorTap = UITapGestureRecognizer()
            manejadorTap(llega: reconocedorTap)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func manejadorEvento(sender: UISwipeGestureRecognizer) {
        
        switch sender.direction {
        case UISwipeGestureRecognizerDirection.right:
            swipeVista.backgroundColor = UIColor.blue
            print("Swipe derecha")

        case UISwipeGestureRecognizerDirection.left:
            swipeVista.backgroundColor = UIColor.red
            print("Swipe izquierda")
        default:
            break
        }
    }
    
    func manejadorTap(llega: UITapGestureRecognizer){
        print("Se hizo tap")
        swipeVista.backgroundColor = UIColor.green
        numeroDeTap += 1
        labelTap.text = String(numeroDeTap)
        
    }

}

